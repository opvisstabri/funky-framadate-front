export class Vote {
	public choice_id: number = 0;
	public value: string = ''; // valeur de réponse

	constructor(choice_id?, value?) {
		if (choice_id) {
			this.choice_id = choice_id;
		}
		if (value) {
			this.value = value;
		}
	}
}
