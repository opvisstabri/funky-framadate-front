import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

@Injectable({
	providedIn: 'root',
})
export class ToastService {
	constructor(private _snackBar: MatSnackBar) {}

	public display(message: string, action?: string): void {
		const config: MatSnackBarConfig = { duration: 4000, verticalPosition: 'top' };
		this._snackBar.open(message, action, config);
	}
}
