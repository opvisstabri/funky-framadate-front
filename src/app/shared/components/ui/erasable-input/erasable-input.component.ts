import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
	selector: 'app-erasable-input',
	templateUrl: './erasable-input.component.html',
	styleUrls: ['./erasable-input.component.scss'],
})
export class ErasableInputComponent implements OnInit {
	@Output() inputModelChange = new EventEmitter();

	@Input() inputModel;

	constructor() {}

	ngOnInit(): void {}

	eraseInput() {
		//
		this.inputModelChange.emit('');
		// TODO focus on other element
	}
}
